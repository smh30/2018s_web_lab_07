// makes the clock appear
var d = new Date();
document.getElementById("clocked").innerHTML = d.getHours()+ ":" + d.getMinutes() + ":" + ("0"+(d.getSeconds())).slice(-2);
function updateTime (){
    var d = new Date();
    document.getElementById("clocked").innerHTML = d.getHours()+ ":" + d.getMinutes() + ":" + ("0"+(d.getSeconds())).slice(-2);
}
//// end of clock
var interval = undefined;

function stop(){
    if (interval !== undefined){
    clearInterval(interval);
    interval = undefined;
}}

function resume(){
    if (interval === undefined){
        interval = setInterval (updateTime, 1000);

    }
}

document.getElementById("stop").addEventListener("click", stop);

document.getElementById("resume").addEventListener("click", resume);

window.addEventListener("load", resume);


//-----------this way below here didn't work, could resume multiple times then can't stop---------------
// var interval = setInterval(updateTime, 1000);
//
//
//
//
// var stop = document.getElementById("stop");
// stop.onclick = function(){clearInterval(interval)};
//
// var resume = document.getElementById("resume");
//
// if(interval !== undefined){
// resume.onclick = function() {
//     updateTime();
//     interval = setInterval(updateTime, 1000);
// };}